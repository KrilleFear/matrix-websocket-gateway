class MatrixWebsocketRequest {
  String type;
  String endpoint;
  Map<String, dynamic> body;
  String txid;
  String accessToken;
  int timeout;

  MatrixWebsocketRequest(
      {this.type,
      this.endpoint,
      this.body,
      this.txid,
      this.accessToken,
      this.timeout});

  MatrixWebsocketRequest.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    endpoint = json['endpoint'];
    body = json['body'];
    txid = json['txid'];
    accessToken = json['access_token'];
    timeout = json['timeout'];
    if (type == null) throw ('Missing argument "type"');
    if (endpoint == null) throw ('Missing argument "endpoint"');
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['type'] = type;
    data['endpoint'] = endpoint;
    if (body != null) {
      data['body'] = body;
    }
    if (txid != null) data['txid'] = txid;
    if (accessToken != null) data['access_token'] = accessToken;
    if (timeout != null) data['timeout'] = timeout;
    return data;
  }
}
