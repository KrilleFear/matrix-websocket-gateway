import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:websocket_server/config.dart';
import 'package:websocket_server/matrix_websocket_request.dart';

void main() {
  HttpServer.bindSecure(
    'janian.de',
    Config.port,
    SecurityContext()
      ..useCertificateChain(Config.certificatePath)
      ..usePrivateKey(Config.privateKey),
  ).then((HttpServer server) {
    print(
        "Matrix Websocket Gateway listening on port ${Config.port} and connecting to ${Config.baseUrl}");
    server.serverHeader = "Matrix Websocket Gateway";
    server.listen((HttpRequest request) {
      if (WebSocketTransformer.isUpgradeRequest(request)) {
        WebSocketTransformer.upgrade(request).then(handleWebSocket);
      } else {
        request.response.statusCode = HttpStatus.forbidden;
        request.response.reasonPhrase = "WebSocket connections only";
        request.response.close();
      }
    });
  });
}

void handleWebSocket(WebSocket socket) {
  print('Client connected!');
  socket.listen((s) async {
    MatrixWebsocketRequest request;
    try {
      request = MatrixWebsocketRequest.fromJson(json.decode(s));
      final headers = {
        if (request.accessToken != null)
          'Authorization': 'Bearer ${request.accessToken}',
        if (request.type == "PUT" || request.type == "POST")
          'Content-Type': 'application/json',
      };
      final timeoutDuration = Duration(seconds: request.timeout ?? 30);
      final url = 'http://' + Config.homeserver + request.endpoint;
      final httpResponse = await (request.type == 'POST'
              ? http.post(
                  url,
                  headers: headers,
                  body: json.encode(request.body),
                )
              : request.type == 'PUT'
                  ? http.put(
                      url,
                      headers: headers,
                      body: json.encode(request.body),
                    )
                  : request.type == 'DELETE'
                      ? http.delete(url, headers: headers)
                      : http.get(url, headers: headers))
          .timeout(timeoutDuration);
      var respBody = httpResponse.body;
      try {
        respBody = utf8.decode(httpResponse.bodyBytes);
      } catch (_) {}
      var jsonString = String.fromCharCodes(respBody.runes);
      if (jsonString.startsWith('[') && jsonString.endsWith(']')) {
        jsonString = '\{"chunk":$jsonString\}';
      }
      request.body = json.decode(jsonString) as Map<String, dynamic>;
      socket.add(json.encode(request.toJson()));
    } catch (e) {
      socket.add('{"errcode":"WEBSOCKET_ERROR","error":"${e.toString()}"}');
    }
  }, onDone: () {
    print('Client disconnected');
  });
}
