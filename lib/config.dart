abstract class Config {
  static const String baseUrl = 'localhost';
  static const String homeserver = 'localhost';
  static const int port = 4042;
  static const String certificatePath = 'path/to/my_cert.pem';
  static const String privateKey = 'path/to/my_key.pem';
}
